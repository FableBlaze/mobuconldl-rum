package rum.algorithms.mobuconldl.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

import RuntimeVerification.RVTempFalse;
import metaconstraints.Compensation;
import metaconstraints.ContexAbsence;
import metaconstraints.DeclareNames;
import metaconstraints.MetaConFormula;
import metaconstraints.ReactiveCompensation;
import rum.algorithms.mobuconldl.MoBuConLdlMonitor;

public class MonitorTest {

	public static void main(String[] args) {

		//String declModelPath = "input/all_constraints.decl";
		//String eventLogPath = "input/Tosem.xes";
		//String declModelPath = "input/MikeModel_Grade5.decl";
		//String eventLogPath = "input/MikeLog_grade5beginning.xes";
		//String declModelPath = "input/sepsis_declareMiner_actSup90.decl";
		//String eventLogPath = "input/Sepsis Cases - Event Log.xes";
		String declModelPath = "input/tosem.decl";
		String eventLogPath = "input/Tosem.xes";
		
		List<MetaConFormula> metaconstraints = new ArrayList<MetaConFormula>();
		
		metaconstraints.add(new ContexAbsence(DeclareNames.RESPONDED_EXISTENCE, "pay", "acc", new RVTempFalse(), "get")); //Matches submission draft
		metaconstraints.add(new ReactiveCompensation(DeclareNames.NOT_COEXISTENCE, "get", "cancel", DeclareNames.EXISTENCE, "return", null)); //Matches submission draft
		metaconstraints.add(new Compensation(DeclareNames.NOT_COEXISTENCE, "get", "cancel", DeclareNames.RESPONSE, "pay", "get")); //Does not match submission draft - using same parapeters as used in draft for conflict and preference
		
		
		MoBuConLdlMonitor monitor = new MoBuConLdlMonitor(false);

		File declModel = new File(declModelPath);
		try {
			monitor.setModel(declModel, metaconstraints);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		XLog xlog = convertToXlog(eventLogPath);
		for (XTrace xtrace : xlog) {
			for (int i = 0; i < xtrace.size()-1; i++) {
				System.out.println(monitor.processNextEvent(xtrace.get(i), false));
			}
			System.out.println(monitor.processNextEvent(xtrace.get(xtrace.size()-1), true));
			System.out.println();
		}
		
		System.out.println("Done");
	}
	
	private static XLog convertToXlog(String logPath) {
		XLog xlog = null;
		File logFile = new File(logPath);

		if (logFile.getName().toLowerCase().endsWith(".mxml")){
			XMxmlParser parser = new XMxmlParser();
			if(parser.canParse(logFile)){
				try {
					xlog = parser.parse(logFile).get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (logFile.getName().toLowerCase().endsWith(".xes")){
			XesXmlParser parser = new XesXmlParser();
			if(parser.canParse(logFile)){
				try {
					xlog = parser.parse(logFile).get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return xlog;
	}
}
