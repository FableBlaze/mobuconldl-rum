package rum.algorithms.mobuconldl.test;

import formula.ldlf.LDLfFormula;
import formula.ltlf.LTLfFormula;
import main.LDLfAutomatonResultWrapper;
import main.LTLfAutomatonResultWrapper;
import main.Main;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;
import rationals.Automaton;
import utils.ParserUtils;

public class FormulasTest {

	public static void main(String[] args) {
		
		//String ldlFormulaString = "(!(<(true)*>((<pay>(tt)) && (!([true](ff)))))) || (<(true)*>((<acc>(tt)) && (!([true](ff)))))";
//		String ldlFormulaString = "[B] (!A || [true](ff))";
//		
//		
//		PropositionalSignature ldlPropositions = new PropositionalSignature();
//		Proposition propositionPay = new Proposition("A");
//		ldlPropositions.add(propositionPay);
//		Proposition propositionAcc = new Proposition("B");
//		ldlPropositions.add(propositionAcc);
//		
//		LDLfAutomatonResultWrapper ldlResult = Main.ldlfString2Aut(ldlFormulaString, ldlPropositions, true, true, false, false);
//		Automaton ldlAutomaton = ldlResult.getAutomaton();
//		
//		System.out.println(ldlAutomaton.toString());
		
		
		//At least logical operators are different from the ones used in RuM
		//String ltlFormulaString = "! ( <> ( ( cancel /\\ X(<>(cancel)) ) ) )";
		//String ltlFormulaString = "! ( <> ( ( cancel && X(<>(cancel)) ) ) )";
		//String ltlFormulaString = "(cancel) && X (false)";
		//String ltlFormulaString = "(cancel && (X false))";
		String ltlFormulaString = "<>((activity1) && ( ! (X(activity1 ||  (!(activity1))))))";
		
		PropositionalSignature ltlPropositions = new PropositionalSignature();
		Proposition propositionCancel = new Proposition("activity1");
		ltlPropositions.add(propositionCancel);
		Proposition propositionReturn = new Proposition("activity2");
		ltlPropositions.add(propositionReturn);
		
		LTLfAutomatonResultWrapper ltlResult = Main.ltlfString2Aut(ltlFormulaString, ltlPropositions, true, true, false, false);
		Automaton ltlAutomaton = ltlResult.getAutomaton();
		
		System.out.println(ltlAutomaton.toString());
		
		
		
		//LTLfFormula ltlFormula = ParserUtils.parseLTLfFormula(ltlFormulaString);
		//LDLfFormula ldlff = ltlFormula.toLDLf();
		//System.out.println(ldlff);
		
	}

}
