package rum.algorithms.mobuconldl.model;

public class DeclareConstraint {
	private String constraintString;
	private DeclareTemplate template;
	private String activationActivity;
	private String targetActivity;
	
	public DeclareConstraint(String constraintString, DeclareTemplate template, String activationActivity, String targetActivity) {
		super();
		this.constraintString = constraintString;
		this.template = template;
		this.activationActivity = activationActivity;
		this.targetActivity = targetActivity;
	}

	public String getConstraintString() {
		return constraintString;
	}
	
	public DeclareTemplate getTemplate() {
		return template;
	}

	public String getActivationActivity() {
		return activationActivity;
	}

	public String getTargetActivity() {
		return targetActivity;
	}

	@Override
	public String toString() {
		return "DeclareConstraint [constraintString=" + constraintString + ", template=" + template
				+ ", activationActivity=" + activationActivity + ", targetActivity=" + targetActivity + "]";
	}
}
