package rum.algorithms.mobuconldl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.model.XEvent;

import RuntimeVerification.ExecutableAutomaton;
import RuntimeVerification.RVTempFalse;
import RuntimeVerification.RVTempTrue;
import RuntimeVerification.RVTrue;
import main.LDLfAutomatonResultWrapper;
import main.LTLfAutomatonResultWrapper;
import main.Main;
import metaconstraints.MetaConFormula;
import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;
import rationals.Automaton;
import rum.algorithms.mobuconldl.model.DeclareConstraint;
import rum.algorithms.mobuconldl.utils.ModelUtils;
import rum.algorithms.mobuconldl.utils.AutomatonUtils;

public class MoBuConLdlMonitor {

	private boolean conflictCheck;

	private List<DeclareConstraint> declareConstrains;
	private List<MetaConFormula> metaConFormulas;
	private Map<String, String> activityToEncoding;
	private PropositionalSignature autAlphabet;
	private Map<DeclareConstraint, Automaton> declareConstraintAutomatonMap = new HashMap<DeclareConstraint, Automaton>();
	private Map<MetaConFormula, Automaton> metaconstraintAutomatonMap = new HashMap<MetaConFormula, Automaton>();
	private List<String> ltlFormulas = new ArrayList<String>();
	private Automaton globalAutomaton;
	private boolean permVioloccurred;

	//Automata for monitoring a specific trace (recreated at trace end)
	private Map<DeclareConstraint, ExecutableAutomaton> declareMonitoringAutomata = new HashMap<DeclareConstraint, ExecutableAutomaton>();
	private Map<MetaConFormula, ExecutableAutomaton> metaconstraintMonitoringAutomata = new HashMap<MetaConFormula, ExecutableAutomaton>();
	private ExecutableAutomaton globalMonitoringAutomaton;


	public MoBuConLdlMonitor(boolean conflictCheck) {
		this.conflictCheck = conflictCheck;
	}


	//Loads the input Declare model and metaconstraints
	//creates individual automata for monitoring
	public void setModel(File declModel, List<MetaConFormula> metaConFormulas) throws IOException {
		this.metaConFormulas = metaConFormulas;

		//Loading constraints from the model
		declareConstrains = ModelUtils.readConstraints(declModel);
		System.out.println("Constraints: " + declareConstrains);


		//Creating activity name encodings to avoid issues with activity names containing dashes etc.
		activityToEncoding = ModelUtils.encodeActivities(declareConstrains);
		System.out.println("Activity encodings: " + activityToEncoding);


		//Creating automaton alphabet
		autAlphabet = ModelUtils.createAutAlphabet(activityToEncoding);


		//Creating automata for monitoring
		System.out.println("Encoded LTL formulas:");
		for (DeclareConstraint declareConstraint : declareConstrains) {
			String ltlFormula = AutomatonUtils.getGenericLtlFormula(declareConstraint.getTemplate());

			//Replacing activity placeholders in the generic formula with activity encodings based on the model
			ltlFormula = ltlFormula.replace("\"A\"", activityToEncoding.get(declareConstraint.getActivationActivity()));
			if (declareConstraint.getTemplate().getIsBinary()) {
				ltlFormula = ltlFormula.replace("\"B\"", activityToEncoding.get(declareConstraint.getTargetActivity()));
			}
			System.out.println("\t" + declareConstraint + ": " + ltlFormula);

			//Creating the constraint automaton, initialising it, and placing it in the automata map
			//Using the full autAlphabet because it is easier to implement, but an automaton-specific subset would be more efficient
			LTLfAutomatonResultWrapper result = Main.ltlfString2Aut(ltlFormula, autAlphabet, true, true, false, false);
			Automaton constraintAutomaton = result.getAutomaton();
			declareConstraintAutomatonMap.put(declareConstraint, constraintAutomaton);
			declareMonitoringAutomata.put(declareConstraint, new ExecutableAutomaton(constraintAutomaton));

			if (conflictCheck) {
				ltlFormulas.add(ltlFormula);
			}
		}

		//Creating global automata for conflict check (does not include metaconstraints)
		if (conflictCheck) {
			String globalLtlFormula = "(" + String.join(") && (", ltlFormulas) + ")";
			LTLfAutomatonResultWrapper result = Main.ltlfString2Aut(globalLtlFormula, autAlphabet, true, true, false, false);
			globalAutomaton = result.getAutomaton();
			globalMonitoringAutomaton = new ExecutableAutomaton(globalAutomaton);
			System.out.println("Global automaton created");
		}
		
		//Using the original activity names for metaconstraints
		PropositionalSignature tmpAlphabet = new PropositionalSignature();
		for (String eventName : activityToEncoding.keySet()) {
			Proposition prop = new Proposition(eventName);
			tmpAlphabet.add(prop);
		}
		tmpAlphabet.add(new Proposition("actx"));
		
		//Creating metaconstraint automata
		System.out.println("Metaconstraints:");
		for (MetaConFormula metaConFormula : metaConFormulas) {
			
			LDLfAutomatonResultWrapper result = Main.ldlfFormula2Aut(metaConFormula.getMetacon(), tmpAlphabet, true, true, false, false);
			Automaton metaconstraintAutomaton = result.getAutomaton();
			metaconstraintAutomatonMap.put(metaConFormula, metaconstraintAutomaton);
			metaconstraintMonitoringAutomata.put(metaConFormula, new ExecutableAutomaton(metaconstraintAutomaton));
			System.out.println("\t" + metaConFormula.getMetacon().toString() + ": " + metaconstraintAutomaton.states().size());
		}
		

		System.out.println("Model processing done!\n\n");
	}



	//Processes the next event in a trace, assumes that processing occurs one trace at a time
	//isTraceEnd=True returns permanent states instead of temporary states and resets automata for monitoring the next trace
	public Map<String, String> processNextEvent(XEvent xevent, boolean isTraceEnd) {
		Map<String, String> constraintStates = new LinkedHashMap<String, String>();

		String eventName = XConceptExtension.instance().extractName(xevent);
		String encodedEventName = activityToEncoding.getOrDefault(eventName, "actx");

		for (DeclareConstraint declareConstraint : declareConstrains) {
			ExecutableAutomaton constraintAutomaton = declareMonitoringAutomata.get(declareConstraint);
			constraintAutomaton.step(encodedEventName);

			MonitoringState monitoringState = AutomatonUtils.getMonitoringState(constraintAutomaton);
			constraintStates.put(declareConstraint.getConstraintString(), monitoringState.getMobuconltlName());
		}
		
		for (MetaConFormula metaConFormula : metaConFormulas) {
			ExecutableAutomaton metaconstraintAutomaton = metaconstraintMonitoringAutomata.get(metaConFormula);

			 //Using the original event name for metaconstraints
			if (activityToEncoding.containsKey(eventName)) {
				metaconstraintAutomaton.step(eventName);
			} else {
				metaconstraintAutomaton.step("actx");
			}
			
			MonitoringState monitoringState = AutomatonUtils.getMonitoringState(metaconstraintAutomaton);
			constraintStates.put(metaConFormula.getMetaconDescription() + "\n\t" + metaConFormula.getMetacon().toString(), monitoringState.getMobuconltlName());
		}

		//Using the global automaton to detect conflicting states
		if (conflictCheck) {
			if (isTraceEnd) {
				globalMonitoringAutomaton = new ExecutableAutomaton(globalAutomaton);
				permVioloccurred = false;
			} else {
				if (!permVioloccurred) {
					for (String monitoringStateString : constraintStates.values()) {
						if ("viol".equals(monitoringStateString)) {
							permVioloccurred = true;
						}
					}
				}

				if (!permVioloccurred) {
					globalMonitoringAutomaton.step(encodedEventName);
					if (!globalMonitoringAutomaton.currentRVTruthValue().getClass().equals(RVTempTrue.class) && !globalMonitoringAutomaton.currentRVTruthValue().getClass().equals(RVTrue.class) && !globalMonitoringAutomaton.currentRVTruthValue().getClass().equals(RVTempFalse.class)) {
						for (String constraint : constraintStates.keySet()) {
							//Marking all constraints (except sat) as conflicting
							if (!constraintStates.get(constraint).equals("sat")) {
								constraintStates.put(constraint, "conflict");
							}
						}
					}
				}
			}
		}

		//All monitoring states become permanent at the end of the trace
		if (isTraceEnd) {
			for (String constraint : constraintStates.keySet()) {
				if (constraintStates.get(constraint).equals("poss.sat")) {
					constraintStates.put(constraint, "sat");
				}
				if (constraintStates.get(constraint).equals("poss.viol")) {
					constraintStates.put(constraint, "viol");
				}
			}

			for (DeclareConstraint declareConstraint : declareConstrains) {
				declareMonitoringAutomata.put(declareConstraint, new ExecutableAutomaton(declareConstraintAutomatonMap.get(declareConstraint)));
			}
			
			for (MetaConFormula metaConFormula : metaConFormulas) {
				metaconstraintMonitoringAutomata.put(metaConFormula, new ExecutableAutomaton(metaconstraintAutomatonMap.get(metaConFormula)));
			}
		}

		return constraintStates;
	}
}
