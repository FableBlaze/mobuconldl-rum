package rum.algorithms.mobuconldl.utils;

import RuntimeVerification.ExecutableAutomaton;
import RuntimeVerification.RVTempFalse;
import RuntimeVerification.RVTempTrue;
import RuntimeVerification.RVTrue;
import rum.algorithms.mobuconldl.MonitoringState;
import rum.algorithms.mobuconldl.model.DeclareTemplate;

public class AutomatonUtils {

	private AutomatonUtils() {
		//Private constructor to avoid unnecessary instantiation of the class
	}

	//Gets the monitoring state that corresponds to the current state of the automaton
	public static MonitoringState getMonitoringState(ExecutableAutomaton constraintAutomaton) {
		if (constraintAutomaton.currentRVTruthValue().getClass().equals(RVTempTrue.class)) {
			return MonitoringState.POSS_SAT;
		} else if (constraintAutomaton.currentRVTruthValue().getClass().equals(RVTrue.class)) {
			return MonitoringState.SAT;
		} else if (constraintAutomaton.currentRVTruthValue().getClass().equals(RVTempFalse.class)) {
			return MonitoringState.POSS_VIOL;
		} else {
			return MonitoringState.VIOL;
		}
	}

	//Returns a generic LTL formula for a given Declare template (formulas are modified to work with FLLOAT)
	public static String getGenericLtlFormula(DeclareTemplate declareTemplate) {
		String formula = "";
		switch (declareTemplate) {
		case Absence:
			formula = "!( <> ( \"A\" ) )";
			break;
		case Absence2:
			formula = "! ( <> ( ( \"A\" && X(<>(\"A\")) ) ) )";
			break;
		case Absence3:
			formula = "! ( <> ( ( \"A\" &&  X ( <> ((\"A\" &&  X ( <> ( \"A\" ) )) ) ) ) ))";
			break;
		case Alternate_Precedence:
			formula = "(((( !(\"B\") U \"A\") || []( !(\"B\"))) && []((\"B\" ->( (!(X(\"A\")) && !(X(!(\"A\"))) ) || X((( !(\"B\") U \"A\") || []( !(\"B\")))))))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) ))";
			break;
		case Alternate_Response:
			formula = "( []( ( \"A\" -> X(( (! ( \"A\" )) U \"B\" ) )) ) )";
			break;
		case Alternate_Succession:
			formula = "( []((\"A\" -> X(( !(\"A\") U \"B\")))) && (((( !(\"B\") U \"A\") || []( !(\"B\"))) && []((\"B\" ->( (!(X(\"A\")) && !(X(!(\"A\"))) ) || X((( !(\"B\") U \"A\") || []( !(\"B\")))))))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )))";
			break;
		case Chain_Precedence:
			formula = "[]( ( X( \"B\" ) -> \"A\") ) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )";
			break;
		case Chain_Response:
			formula = "[] ( ( \"A\" -> X( \"B\" ) ) )";
			break;
		case Chain_Succession:
			//This formula does not work correctly with the FLLOAT automata library (incorrect results)
			formula = "( []( ( \"A\" -> X( \"B\" ) ) )) && ([]( ( X( \"B\" ) ->  \"A\") ) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) ) )";
			break;
		case Choice:
			formula = "(  <> ( \"A\" ) || <>( \"B\" )  )";
			break;
		case CoExistence:
			formula = "( ( <>(\"A\") -> <>( \"B\" ) ) && ( <>(\"B\") -> <>( \"A\" ) )  )";
			break;
		case End:
			//formula = "(\"A\") && !X (true)";
			//formula = "<>( (\"A\") && !X (true))";
			//formula = "<>((A) && ( ! (X(A ||  (!(A))))))";
			formula = "( <> ( \"A\" && !( X( \"A\" U ( !\"A\" ) ) ) ) )";
			break;
		case Exactly1:
			formula = "(  <> (\"A\") && ! ( <> ( ( \"A\" && X(<>(\"A\")) ) ) ) )";
			break;
		case Exactly2:
			formula = "( <> (\"A\" && (\"A\" -> (X(<>(\"A\"))))) &&  ! ( <>( \"A\" && (\"A\" -> X( <>( \"A\" && (\"A\" -> X ( <> ( \"A\" ) ))) ) ) ) ) )";
			break;
		case Exclusive_Choice:
			formula = "(  ( <>( \"A\" ) || <>( \"B\" )  )  && !( (  <>( \"A\" ) && <>( \"B\" ) ) ) )";
			break;
		case Existence:
			formula = "( <> ( \"A\" ) )";
			break;
		case Existence2:
			formula = "<> ( ( \"A\" && X(<>(\"A\")) ) )";
			break;
		case Existence3:
			formula = "<>( \"A\" && X(  <>( \"A\" && X( <> \"A\" )) ))";
			break;
		case Init:
			formula = "( \"A\" )";
			break;
		case Not_Chain_Precedence:
			formula = "[] ( \"A\" -> !( X ( \"B\" ) ) )";
			break;
		case Not_Chain_Response:
			formula = "[] ( \"A\" -> !( X ( \"B\" ) ) )";
			break;
		case Not_Chain_Succession:
			formula = "[]( ( \"A\" -> !(X( \"B\" ) ) ))";
			break;
		case Not_CoExistence:
			formula = "(<>( \"A\" )) -> (!(<>( \"B\" )))";
			break;
		case Not_Precedence:
			formula = "[] ( \"A\" -> !( <> ( \"B\" ) ) )";
			break;
		case Not_Responded_Existence:
			formula = "(<>( \"A\" )) -> (!(<>( \"B\" )))";
			break;
		case Not_Response:
			formula = "[] ( \"A\" -> !( <> ( \"B\" ) ) )";
			break;
		case Not_Succession:
			formula = "[]( ( \"A\" -> !(<>( \"B\" ) ) ))";
			break;
		case Precedence:
			formula = "( ! (\"B\" ) U \"A\" ) || ([](!(\"B\"))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )";
			break;
		case Responded_Existence:
			formula = "(( ( <>( \"A\" ) -> (<>( \"B\" ) )) ))";
			break;
		case Response:
			formula = "( []( ( \"A\" -> <>( \"B\" ) ) ))";
			break;
		case Succession:
			formula = "(( []( ( \"A\" -> <>( \"B\" ) ) ))) && (( ! (\"B\" ) U \"A\" ) || ([](!(\"B\"))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )   )";
			break;
		default:
			break;
		}
		return formula;
	}
}
