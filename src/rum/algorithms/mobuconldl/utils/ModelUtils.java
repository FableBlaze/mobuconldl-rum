package rum.algorithms.mobuconldl.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.tweety.logics.pl.syntax.Proposition;
import net.sf.tweety.logics.pl.syntax.PropositionalSignature;
import rum.algorithms.mobuconldl.model.DeclareConstraint;
import rum.algorithms.mobuconldl.model.DeclareTemplate;

public class ModelUtils {

	//Private constructor to avoid unnecessary instantiation of the class
	private ModelUtils() {
	}

	//Finds constraint strings in the Declare model and creates a list of Declare constraint objects
	public static List<DeclareConstraint> readConstraints(File declareModelPath) throws IOException {
		List<DeclareConstraint> declareConstraints = new ArrayList<DeclareConstraint>();

		Scanner sc = new Scanner(declareModelPath);
		Pattern constraintPattern = Pattern.compile("\\w+(\\[.*\\]) \\|");

		while(sc.hasNextLine()) {
			String line = sc.nextLine();

			if(line.startsWith("activity") && line.length() > 9) {
				//Skipping activity definitions
			} else if(line.startsWith("bind") && line.length() > 7 && line.substring(6).contains(":")) {
				//Skipping activity-attribute bindings
			} else {
				Matcher constraintMatcher = constraintPattern.matcher(line);
				if (constraintMatcher.find()) { //Constraints
					DeclareConstraint declareConstraint = readConstraintString(line);
					declareConstraints.add(declareConstraint);
				}
			}
		}
		sc.close();

		return declareConstraints;
	}

	//Creates a single Declare constraint object from Declare constraint string
	public static DeclareConstraint readConstraintString(String constraintString) {
		DeclareTemplate template = null;
		String activationActivity = null;
		String targetActivity = null;
		
		Matcher mBinary = Pattern.compile("(.*)\\[(.*), (.*)\\]").matcher(constraintString);
		Matcher mUnary = Pattern.compile(".*\\[(.*)\\]").matcher(constraintString);

		//Processing the constraint
		if(mBinary.find()) { //Binary constraints
			template = DeclareTemplate.getByTemplateName(mBinary.group(1));
			if(template.getReverseActivationTarget()) {
				targetActivity = mBinary.group(2);
				activationActivity = mBinary.group(3);
			}
			else {
				activationActivity = mBinary.group(2);
				targetActivity = mBinary.group(3);
			}
			constraintString = mBinary.group(1) + "[" + mBinary.group(2) + ", " + mBinary.group(3) + "]";
		} else if(mUnary.find()) { //Unary constraints
			String templateString = mUnary.group(0).substring(0, mUnary.group(0).indexOf("[")); //TODO: Should be done more intelligently
			template = DeclareTemplate.getByTemplateName(templateString);
			activationActivity = mUnary.group(1);
			constraintString = template + "[" + mUnary.group(1) + "]";
		}
		
		return new DeclareConstraint(constraintString, template, activationActivity, targetActivity);
	}

	public static Map<String, String> encodeActivities(List<DeclareConstraint> declareConstrains) {
		Map<String, String> activityToEncoding = new HashMap<String, String>();
		
		for (DeclareConstraint declareConstraint : declareConstrains) {
			String activity = declareConstraint.getActivationActivity();
			if (!activityToEncoding.containsKey(activity)) {
				activityToEncoding.put(activity, "act" + activityToEncoding.size());
			}
			if (declareConstraint.getTemplate().getIsBinary()) {
				activity = declareConstraint.getTargetActivity();
				if (!activityToEncoding.containsKey(activity)) {
					activityToEncoding.put(activity, "act" + activityToEncoding.size());
				}
			}
		}
		return activityToEncoding;
	}

	public static PropositionalSignature createAutAlphabet(Map<String, String> activityToEncoding) {
		PropositionalSignature autAlphabet = new PropositionalSignature();
		
		for (String activityEncoding : activityToEncoding.values()) {
			Proposition prop = new Proposition(activityEncoding);
			autAlphabet.add(prop);
		}
		
		autAlphabet.add(new Proposition("actx")); //Used for all activities that are not present in the declare model and in metaconstraints
		
		return autAlphabet;
	}
}
